### How to use this Calendar:

- Navigate to your orientation start date.
- You will see listed a series of events at different times.
- Click on the calendar event and event details will pop up.
- On the calendar details, you will see a Zoom URL ("link") that you will use to join the event.

### How to add this calendar to your Google Calendar, iCalendar, or Outlook:

- The Public URL for this calendar is [here](https://calendar.google.com/calendar/embed?src=c_ngr102dnk2044da84oj3lm8uc4%40group.calendar.google.com&ctz=America%2FLos_Angeles).
- If you already use Google calendar, click on the `+Google Calendar` on the icon on the bottom right. This will add this calendar to your calendar.
- To add this Calendar to your Mac device, [follow these instructions](https://support.apple.com/guide/calendar/subscribe-to-calendars-icl1022/mac).
- To add this Calendar to Outlook, [follow these instructions](https://www.howtogeek.com/408364/how-to-show-a-google-calendar-in-outlook/). You will need the private [iCal link here](https://calendar.google.com/calendar/ical/c_ngr102dnk2044da84oj3lm8uc4%40group.calendar.google.com/private-98e73197f284ec4ad8dbc92b0247953b/basic.ics) to complete the process.

<hr>
<div class="calendar-container"></div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.7/jstz.min.js"></script>
<script>
{
    const calendarContainer = document.querySelector('.calendar-container');
    const possibleTimezone = encodeURIComponent(jstz.determine().name());
    const fallbackTimezone = 'America/Indiana/Indianapolis';

    const timezoneDetectionSucceeded = possibleTimezone && (possibleTimezone !== String('false')); // Based on a very superficial glance at the source code of jstimezonedetect, it looks like it *may* be possible for us to get back either a falsy value or the string "false": https://github.com/pellepim/jstimezonedetect/commit/ddc9e040342d68c06129ae28011517e5b2dde664?branch=ddc9e040342d68c06129ae28011517e5b2dde664&diff=unified#diff-ab641f208e2d305fcf750c52c2da553ff0a1276c78d389fc8bff6a61686c35c9R409
    const timezone = timezoneDetectionSucceeded ? possibleTimezone : fallbackTimezone;
    const calendarURL = `https://calendar.google.com/calendar/embed?src=c_ngr102dnk2044da84oj3lm8uc4%40group.calendar.google.com&ctz=${timezone}`;

    calendarContainer.innerHTML = `
        <iframe
            src="${calendarURL}" 
            width="800" 
            height="600" 
            frameborder="0" 
            scrolling="no" 
            style="border-width: 0;"
        ></iframe>
    `;
}
</script>

<details>
    <summary>Is the calendar not correctly detecting your timezone? Click here to view it in Pacific Time.</summary>
    <iframe src="https://calendar.google.com/calendar/embed?src=c_ngr102dnk2044da84oj3lm8uc4%40group.calendar.google.com&amp;ctz=America%2FLos_Angeles" width="800" height="600" frameborder="0" scrolling="no" style="border-width: 0;"></iframe>
</details>

<details>
    <summary>Is the calendar not correctly detecting your timezone? Click here to view it in Eastern Time.</summary>
    <iframe src="https://calendar.google.com/calendar/embed?src=c_ngr102dnk2044da84oj3lm8uc4%40group.calendar.google.com&amp;ctz=America%2FIndiana%2FIndianapolis" width="800" height="600" frameborder="0" scrolling="no" style="border-width: 0;"></iframe>
</details>
